define nginx::drupal-7 (
  $ensure = 'present',
  $root = '/var/www', 
) {
  include nginx

  # $link_ensure = $ensure ? {
  #   'present' => 'link',
  #   default => 'absent',
  # }

  $file_ensure = $ensure ? {
    'present' => 'file',
    default => 'absent',
  }

  file { ["/etc/nginx/apps", "/etc/nginx/apps/drupal", "/var/cache/nginx"] :
    ensure => "directory",
  }

  file { "/etc/nginx/fastcgi_private_files.conf":
    ensure  => $file_ensure,
    content => template("nginx/conf/fastcgi_private_files.conf.erb"),
    #require => File["/etc/nginx/fastcgi_private_files.conf"],
    notify  => Service['nginx'],
  }

  file { "/etc/nginx/fastcgi_drupal.conf":
    ensure  => $file_ensure,
    content => template("nginx/conf/fastcgi_drupal.conf.erb"),
    #require => File["/etc/nginx/fastcgi_drupal.conf"],
    notify  => Service['nginx'],
  }

  file { "/etc/nginx/apps/drupal/drupal.conf":
    ensure  => $file_ensure,
    content => template("nginx/conf/apps/drupal/drupal.conf.erb"),
    #require => File["/etc/nginx/apps/drupal/drupal.conf"],
    notify  => Service['nginx'],
  }

  file { "/etc/nginx/apps/drupal/microcache_fcgi.conf":
    ensure  => $file_ensure,
    content => template("nginx/conf/apps/drupal/microcache_fcgi.conf.erb"),
    #require => File["/etc/nginx/apps/drupal/microcache_fcgi.conf"],
    notify  => Service['nginx'],
  }

  # Symlink
  # file { "/etc/nginx/sites-enabled/${priority}-${file}.conf":
  #   ensure  => $link_ensure,
  #   target  => "/etc/nginx/sites-available/${priority}-${file}.conf",
  #   require => Package['nginx'],
  # }


  # Avec template
  # file { "/etc/nginx/sites-available/${priority}-${file}.conf":
  #   ensure  => $file_ensure,
  #   content => template($template),
  #   require => File["/etc/nginx/sites-enabled/${priority}-${file}.conf"],
  #   notify  => Service['nginx'],
  # }

  # supression de fichier
  # file {"rm-nginx-default":
  #   path => '/etc/nginx/sites-enabled/default',
  #   ensure => absent,
  #   require => Package['nginx'],  
  # }
}