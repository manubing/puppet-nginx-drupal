define nginx::conf (
  $ensure = 'present',
) {
  include nginx

  $link_ensure = $ensure ? {
    'present' => 'link',
    default => 'absent',
  }

  $file_ensure = $ensure ? {
    'present' => 'file',
    default => 'absent',
  }

  file { "/etc/nginx/nginx.conf":
    ensure  => $file_ensure,
    content => template("nginx/conf/nginx.conf.erb"),
    #require => File["/etc/nginx/nginx.conf"],
    notify  => Service['nginx'],
  }

  file { "/etc/nginx/fastcgi.conf":
    ensure  => $file_ensure,
    content => template("nginx/conf/fastcgi.conf.erb"),
    notify  => Service['nginx'],
  }

  file { "/etc/nginx/upstream_phpcgi_tcp.conf":
    ensure  => $file_ensure,
    content => template("nginx/conf/upstream_phpcgi_tcp.conf.erb"),
    notify  => Service['nginx'],
  }

  file { "/etc/nginx/upstream_phpcgi_unix.conf":
    ensure  => $file_ensure,
    content => template("nginx/conf/upstream_phpcgi_unix.conf.erb"),
    notify  => Service['nginx'],
  }
  
  file { "/etc/nginx/map_block_http_methods.conf":
    ensure  => $file_ensure,
    content => template("nginx/conf/map_block_http_methods.conf.erb"),
    notify  => Service['nginx'],
  }

  file { "/etc/nginx/nginx_status_allowed_hosts.conf":
    ensure  => $file_ensure,
    content => template("nginx/conf/nginx_status_allowed_hosts.conf.erb"),
    notify  => Service['nginx'],
  }

  file { "/etc/nginx/blacklist.conf":
    ensure  => $file_ensure,
    content => template("nginx/conf/blacklist.conf.erb"),
    notify  => Service['nginx'],
  }
  
  file { "/etc/nginx/map_cache.conf":
    ensure  => $file_ensure,
    content => template("nginx/conf/map_cache.conf.erb"),
    notify  => Service['nginx'],
  }

  file { "/etc/nginx/fastcgi_microcache_zone.conf":
    ensure  => $file_ensure,
    content => template("nginx/conf/fastcgi_microcache_zone.conf.erb"),
    notify  => Service['nginx'],
  }

}